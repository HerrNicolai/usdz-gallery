# USDZ Gallery

There are some images, that have been extracted from bigger paintings, for which the size of the original is given instead of the dimension of the extracted part. For Google Art Project paintings, the description includes this text

This file has been extracted from another file

Example: https://commons.wikimedia.org/wiki/File:Pierre-Auguste_Renoir_-_Luncheon_of_the_Boating_Party_-_Google_Art_Project_(Alphonsine_Fournaise).jpg

I will have to update the list to remove these items
